// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
  pub val: i32,
  pub next: Option<Box<ListNode>>
}

impl ListNode {
  #[inline]
  fn new(val: i32) -> Self {
    ListNode {
      next: None,
      val
    }
  }
}

pub fn add_two_numbers(l1: Option<Box<ListNode>>, l2: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
    let mut p = l1;
    let mut q = l2;
    let mut p3 = None;
    let mut l3 = &mut p3;
    let mut leftover = 0;
    loop {
        if p == None && q == None {
            if leftover != 0 {
                *l3 = Some(Box::new(ListNode::new(leftover)));
            }
            break p3;
        }
        let mut value = 0;
        if let Some(option) = p {
            value = option.val;
            p = option.next;
        }
        if let Some(option) = q {
            value += option.val;
            q = option.next;
        }
        *l3 = Some(Box::new(ListNode::new((value+leftover)%10)));
        leftover = (value+leftover)/10;
        if let Some(list) = l3 {
           l3 = &mut list.next;
        }
    }
}

fn main() {
    let a = Some(Box::new(ListNode {
        val: 2,
        next: Some(Box::new(ListNode {
            val: 3,
            next: Some(Box::new(ListNode::new(4))),
        })),
    }));
    let b = Some(Box::new(ListNode {
        val: 9,
        next: Some(Box::new(ListNode {
            val: 8,
            next: Some(Box::new(ListNode {
                val: 7,
                next: Some(Box::new(ListNode::new(6))),
            })),
        })),
    }));
    let mut c = add_two_numbers(a,b);
    while let Some(i) = c {
        c = i.next;
        println!("{}",i.val);
    }
}    
